import { Client } from "discord.js";
import { CommandError, CommandManager } from "@jiman24/commandment";
import { config } from "dotenv";
import path from "path";
import { CooldownManager } from "./structure/CooldownManager";
import { PlayerManager } from "./structure/PlayerManager";
import { MessageEmbed } from "./structure/MessageEmbed";
import { RoleManager } from "./structure/RoleManager";
import { SettingsManager } from "./structure/SettingsManager";

config();

export const client = new Client({ intents: ["GUILDS", "GUILD_MESSAGES"] });
export const cdManager = new CooldownManager();
export const playerManager = new PlayerManager();
export const roleManager = new RoleManager();
export const settingsManager = new SettingsManager();

const COMMAND_PREFIX = "!";
const commandManager = new CommandManager(COMMAND_PREFIX);

commandManager.verbose = true;
commandManager.registerCommands(path.resolve(__dirname, "./commands"));

commandManager.registerCommandErrorHandler((err, msg) => {
  
  if (err instanceof CommandError) {

    const embed = new MessageEmbed(msg.author);
    embed.setDescription(err.message);
    embed.setColor("RED");
    msg.channel.send({ embeds: [embed] });

  } else {
    console.log(err);
  }
})

commandManager.registerCommandNotFoundHandler((msg, cmdName) => {
  msg.channel.send(`Cannot find command "${cmdName}"`);
})

commandManager.registerCommandOnCooldownHandler((msg, cmd, timeLeft) => {
  const { hours, minutes, seconds } = timeLeft;

  msg.channel.send(
    `You cannot run ${cmd.name} command after **${hours}h ${minutes}m ${seconds}s**`
  );
})

commandManager.registerCommandMissingPermissionHandler((msg, perms) => {
  msg.channel.send(`Missing permissions \`${perms.join(", ")}\``);
})

client.on("ready", () => console.log(client.user?.username, "is ready!"))
client.on("messageCreate", async msg => { 

  if (msg.content.startsWith("!") && msg.content.length > 1) {


    if (settingsManager.interactChannelID.length > 0) {

      if (msg.channel.id !== settingsManager.interactChannelID) {
        return;
      }

    }
  }

  commandManager.handleMessage(msg);
});

