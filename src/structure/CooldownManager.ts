import { DateTime, DurationLikeObject } from "luxon";
import Josh from "@joshdb/core";
//@ts-ignore
import provider from "@joshdb/sqlite";

export class CooldownManager {
  private db: Josh<Date>;

  constructor() {
    this.db = new Josh({
      name: "cooldowns",
      provider,
    });
  }

  private isPassed(dt: DateTime | Date) {

    if (dt instanceof Date) {
      dt = DateTime.fromJSDate(dt);
    }


    return dt.diffNow(["seconds"]).seconds < 0;
  }

  private id(commandID: string, userID: string) {
    return `${commandID}-${userID}`;
  }

  setCooldown(commandID: string, userID: string, duration: DurationLikeObject) {
    const time = DateTime.now().plus(duration);
    this.db.set(this.id(commandID, userID), time.toJSDate());
  }

  async isOnCooldown(commandID: string, userID: string) {

    const lastSet = await this.db.get(this.id(commandID, userID));

    if (!lastSet) return false;

    return !this.isPassed(lastSet);
  }


  async getTimeLeft(commandID: string, userID: string): Promise<string | "ready"> {

    const lastSet = await this.db.get(this.id(commandID, userID));

    if (!lastSet) return "ready";

    const dt = DateTime.fromJSDate(lastSet);

    if (this.isPassed(dt)) return "ready";

    const { 
      hours, 
      minutes, 
      seconds,
    } = dt.diffNow(["hours", "minutes", "seconds", "milliseconds"]);

    return `${hours} hours, ${minutes} mins, ${seconds} secs`
  }
}
