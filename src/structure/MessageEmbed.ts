import { User } from "discord.js";
import { MessageEmbed as Embed } from "discord.js";
import { cap } from "@jiman24/discordjs-utils";

export class MessageEmbed extends Embed {
  user: User;

  constructor(user: User) {
    super();
    this.user = user;

    this.setAuthor({ 
      name: this.user.username, 
      iconURL: this.user.displayAvatarURL(),
    });

    this.setColor("RANDOM");
  }

  setDescription(description: string) {

    super.setDescription(cap(description));

    return this;
  }
}
