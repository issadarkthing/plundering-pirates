import Josh from "@joshdb/core";
//@ts-ignore
import provider from "@joshdb/sqlite";
import { Guild, Message } from "discord.js";
import { roleManager, settingsManager } from "../bot";
import { MessageEmbed } from "../structure/MessageEmbed";
import { Player } from "./Player";

interface Role {
  // discord's role id
  id: string;
  // give this role if player's coin exceeds or equal to this threshold
  threshold: number;
}

export async function handleRole(msg: Message, player: Player, embed: MessageEmbed) {

  if (!settingsManager.enableAutoRole) return;

  const validRoles = await roleManager.validRoles(player.coins);

  await msg.guild!.fetch();

  const discordRoles = roleManager.getValidRoles(msg.guild!, validRoles);
  const member = msg.member!;

  for (const role of discordRoles) {
    if (member.roles.cache.has(role.id)) continue;

    member.roles.add(role);
    embed.setDescription(`You received ${role}`);
    msg.channel.send({ embeds: [embed] });
  }
}

export class RoleManager {
  private db: Josh<Role>;

  constructor() {
    this.db = new Josh({
      name: "roles",
      provider,
    });
  }

  getAll() {
    return this.db.values;
  }

  hasRole(id: string) {
    return this.db.has(id);
  }

  async validRoles(amount: number) {
    const roles = await this.db.values;
    return roles.filter(x => amount >= x.threshold).map(x => x.id);
  }

  getValidRoles(guild: Guild, roles: string[]) {
    return [...guild.roles.cache.filter(x => roles.includes(x.id)).values()];
  }

  async addRole(role: Role) {
    await this.db.set(role.id, role);
  }

  async removeRole(id: string) {
    await this.db.delete(id);
  }
}
