import Josh from "@joshdb/core";
//@ts-ignore
import provider from "@joshdb/sqlite";

export class SettingsManager {
  private db: Josh;
  private readonly id: string;
  enableAutoRole: boolean;
  interactChannelID: string;

  constructor() {
    this.id = "main";
    this.db = new Josh({
      name: "settings",
      provider,
    });

    this.enableAutoRole = true;
    this.interactChannelID = ""; // set to empty string to disable

    this.db.get(this.id)
      .then((data) => {
        Object.assign(this, data);
      })
  }

  save() {
    const { db, ...data } = this;
    this.db.set(this.id, data);
  }
}
