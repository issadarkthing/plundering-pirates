import Josh from "@joshdb/core";
//@ts-ignore
import provider from "@joshdb/sqlite";


export class PlayerManager {
  db = new Josh({
    name: "players",
    provider,
  });
}
