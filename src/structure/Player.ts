import { User } from "discord.js";
import { playerManager } from "../bot";

export class Player {
  id: string;
  name: string;
  coins = 0;

  constructor(id: string, name: string) {
    this.id = id;
    this.name = name;
  }

  static async fromUser(user: User) {
    const player = new Player(user.id, user.username);
    const data = await playerManager.db.get(player.id);

    Object.assign(player, data);

    return player;
  }

  async save() {
    await playerManager.db.set(this.id, { ...this });
  }
}
