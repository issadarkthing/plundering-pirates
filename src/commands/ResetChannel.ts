import { Message, PermissionResolvable } from "discord.js";
import { Command } from "@jiman24/commandment";
import { settingsManager } from "../bot";
import { MessageEmbed } from "../structure/MessageEmbed";

export default class extends Command {
  name = "reset-channel";
  description = "Allow bot to interact to all channels";
  permissions: PermissionResolvable[] = ["ADMINISTRATOR"];

  async exec(msg: Message) {

    settingsManager.interactChannelID = "";
    settingsManager.save();

    const embed = new MessageEmbed(msg.author)
      .setDescription(`successfully allow bot to interact to all channel`);

    this.sendEmbed(msg, embed);
  }
}
