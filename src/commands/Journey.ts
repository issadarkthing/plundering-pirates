import { Message } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { oneLine } from "common-tags";
import { Player } from "../structure/Player";
import { cdManager } from "../bot";
import { random } from "@jiman24/discordjs-utils";
import { MessageEmbed } from "../structure/MessageEmbed";
import { ButtonHandler } from "@jiman24/discordjs-button";
import { handleRole } from "../structure/RoleManager";
import { coin } from "../structure/constants";

export default class extends Command {
  name = "journey";
  description = "Set sails on perilous adventures!";
  minEarn = -10;
  maxEarn = 15;
  lores = [
    oneLine`A ship spotted in the sea waving white flags. Are ye in trouble? Is
    it a trap? The navy? You head forth to inspect.`,
    oneLine`While rummaging through the remains of an abandoned aztec ship, you
    encounter a chest with a skull branded on top. Cursed gold? or priceless
    treasures? You decide to take the chance.`,
    oneLine`The crewmates speak of a mermaid in the waters, could it be? You
    decide to give chase, hoping the rumors of man-eating mermaids to be just
    that. Rumors. What shall ye find?`,
  ];

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);

    if (await cdManager.isOnCooldown(this.name, player.id)) {
      const timeLeft = await cdManager.getTimeLeft(this.name, player.id);
      throw new CommandError(`You are on cooldown. Please wait for ${timeLeft}`);
    }

    const lore = random.pick(this.lores);
    const loot = random.integer(this.minEarn, this.maxEarn);
    const embed = new MessageEmbed(msg.author);

    embed.setDescription(lore);

    const menu = new ButtonHandler(msg, embed);

    let collected = false;
  
    menu.addButton("Collect", () => {
      collected = true;
    });

    await menu.run();

    if (!collected) {
      throw new CommandError("reward has not been claimed");
    }

    if (loot >= 0) {
      embed.setDescription(`You've earned ${loot} ${coin}`);
    } else {
      embed.setDescription(`You lost ${loot} ${coin}`);
    }

    this.sendEmbed(msg, embed);

    player.coins += loot;
    player.save();

    if (loot >= 0) {
      await handleRole(msg, player, embed);
    }

    cdManager.setCooldown(this.name, player.id, { minutes: 30 });
  }
}
