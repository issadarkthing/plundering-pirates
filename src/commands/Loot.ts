import { Message } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { MessageEmbed } from "../structure/MessageEmbed";
import { Player } from "../structure/Player";
import { coin } from "../structure/constants";
import { ButtonHandler } from "@jiman24/discordjs-button";
import { random, sleep } from "@jiman24/discordjs-utils";
import { cdManager } from "../bot";
import { handleRole } from "../structure/RoleManager";
import { oneLine } from "common-tags";

export default class extends Command {
  name = "loot";
  description = `Loot yer chest and gain ${coin}!`;
  maxEarn = 75;
  lores = [
    oneLine`X marks the spot where the treasure lies. The map leads you to the
    most dangerous place in the ocean: the Bermuda Triangle. After facing the
    most brutal waters you have ever faced, you arrive at the island marked on
    the map. Hours of digging finally reveals the treasure chest containing
    riches beyond your wildest dreams.`,
  ];

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);

    if (await cdManager.isOnCooldown(this.name, player.id)) {
      const timeLeft = await cdManager.getTimeLeft(this.name, player.id);
      throw new CommandError(`You are on cooldown. Please wait for ${timeLeft}`);
    }

    const embed = new MessageEmbed(msg.author);
    embed.setColor("GREEN");
    embed.setImage("https://cdn.discordapp.com/attachments/921236230220447835/959025249775673364/chest2-removebg-preview.png");
    embed.setDescription(random.pick(this.lores) + "\nYou found a chest! Only you can open this chest");

    const menu = new ButtonHandler(msg, embed);

    let collected = false;

    menu.addButton("Collect", () => {
      collected = true;
    });

    await menu.run();

    if (!collected) {
      throw new CommandError("chest has not been claimed");
    }

    const loadingMsg = await msg.channel.send("Opening...");

    await sleep(1000);

    const loot = random.integer(0, this.maxEarn);

    player.coins += loot;
    player.save();

    embed.setImage("");
    embed.setDescription(`You've earned ${loot} ${coin}`);

    await loadingMsg.delete();

    this.sendEmbed(msg, embed);

    await handleRole(msg, player, embed);

    cdManager.setCooldown(this.name, player.id, { hours: 24 });
  }
}
