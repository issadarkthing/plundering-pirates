import { Message, PermissionResolvable } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { roleManager } from "../bot";
import { MessageEmbed } from "../structure/MessageEmbed";

export default class extends Command {
  name = "remove-role";
  description = "Remove an auto role";
  permissions: PermissionResolvable[] = ["ADMINISTRATOR"];

  async exec(msg: Message) {

    const role = msg.mentions.roles.first();

    if (!role) {
      throw new CommandError("you need to mention a role");
    } 

    const isAutoRole = await roleManager.hasRole(role.id);

    if (!isAutoRole) {
      throw new CommandError(`${role} is not in auto role list`);
    }

    await roleManager.removeRole(role.id);

    const embed = new MessageEmbed(msg.author);
    embed.setColor("GREEN");

    embed.setDescription(`Succesfully removed ${role}`);

    this.sendEmbed(msg, embed);

  }
}
