import { Message } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { coin } from "../structure/constants";
import { Player } from "../structure/Player";
import { cdManager } from "../bot";
import { random } from "@jiman24/discordjs-utils";
import { MessageEmbed } from "../structure/MessageEmbed";
import { handleRole } from "../structure/RoleManager";
import { oneLine } from "common-tags";
import { ButtonHandler } from "@jiman24/discordjs-button";

export default class extends Command {
  name = "fight";
  description = `Engange in battle and win ${coin}!`;
  minEarn = 5;
  maxEarn = 10;
  lores = [
    oneLine`Lookout has spotted another pirate shipping sailing directly for us
    and they do not look friendly. The crew starts loading up the cannons and
    preparing for battle. After several hours of cannonballs flying, you watch
    the burning enemy ship sink into the sea.`,
    oneLine`After a long trip at sea, you and your mates are enjoying a nice
    drink on land. Before you can even finish the first bottle of rum, a group
    of navy officers walk in the door of the tavern. A storm of glass bottles
    and wooden chairs went flying at the officers as the fight broke out. You
    manage to take care of them quick enough to have time for another drink.`,
    oneLine`The Kraken used to nothing but a legend until the ships started
    disappearing one by one. Nobody has seen the Kraken and lived to tell the
    tale, but the reward for slaying it was too good to pass up. Every member of
    the crew is prepared to go down with the ship as we sail to where the ship
    was last seen. Several days later, your damaged ship arrives back to port
    with the head of the monster that terrorized the seas.`,
  ];

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);

    if (await cdManager.isOnCooldown(this.name, player.id)) {
      const timeLeft = await cdManager.getTimeLeft(this.name, player.id);
      throw new CommandError(`You are on cooldown. Please wait for ${timeLeft}`);
    }

    const loot = random.integer(this.minEarn, this.maxEarn);
    const embed = new MessageEmbed(msg.author);

    embed.setDescription(random.pick(this.lores));

    const menu = new ButtonHandler(msg, embed);

    let collected = false;
  
    menu.addButton("Collect", () => {
      collected = true;
    });

    await menu.run();

    if (!collected) {
      throw new CommandError("reward has not been claimed");
    }


    embed.setDescription(`You've earned ${loot} ${coin}`);
    this.sendEmbed(msg, embed);

    player.coins += loot;
    player.save();

    await handleRole(msg, player, embed);

    cdManager.setCooldown(this.name, player.id, { hours: 3 });
  }
}
