import { Message } from "discord.js";
import { Command } from "@jiman24/commandment";
import { cdManager } from "../bot";
import { MessageEmbed } from "../structure/MessageEmbed";

export default class extends Command {
  name = "cooldown";
  aliases = ["cd"];
  description = "Show all your cooldowns";

  async exec(msg: Message) {

    const embed = new MessageEmbed(msg.author);
    const commands = ["plunder", "fight", "loot"];
    const cooldowns: string[] = [];

    for (const command of commands) {
      const cooldown = await cdManager.getTimeLeft(command, msg.author.id);
      cooldowns.push(cooldown);
    }

    const prefix = this.commandManager.prefix;

    const lines = cooldowns.map((timeLeft, i) => {
      const command = commands[i];
      const cmd = `\`${prefix}${command}\` - `;

      if (timeLeft === "ready") {
        return cmd + "ready";
      }

      return cmd + `ready in ${timeLeft}`;
    });

    embed.setDescription(lines.join("\n"));

    this.sendEmbed(msg, embed);
  }
}
