import { Message } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { cdManager } from "../bot";
import { Player } from "../structure/Player";
import { MessageEmbed } from "../structure/MessageEmbed";
import { random } from "@jiman24/discordjs-utils";
import { coin } from "../structure/constants";
import { handleRole } from "../structure/RoleManager";
import { oneLine } from "common-tags";
import { ButtonHandler } from "@jiman24/discordjs-button";

export default class extends Command {
  name = "plunder";
  description = `Plunder and pirate to earn ${coin}!`;
  minEarn = 15;
  maxEarn = 25;
  lores = [
    oneLine`You spy a defenseless little cargo ship. Looks like they are having
    trouble carrying all that cargo, we are going to do them a favor and it take
    off their hands. After boarding the cargo ship, you have its crew move
    everything to your ship. Since the cargo ship crew was so cooperative, you
    decide to let them escape with their lives.`,
    oneLine`The ship carrying the governor’s daughter will be coming through
    here very soon. Using a surprise attack, you manage to damage the ship and
    kidnap the girl. Knowing that he could not take any risks, the governor
    sends a single man by row boat to deliver the ransom in exchange for the
    girl`,
    oneLine`Somebody in the tavern had a little bit too much to drink and will
    not shut up about the new ship he just purchased. You buy him a few more
    drinks while he tells you everything about it including the fact that nobody
    is aboard right now. You pass along the information to your first mate while
    the drunk isn’t paying attention, and add a shiny new ship to your fleet.`,
  ];

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);

    if (await cdManager.isOnCooldown(this.name, player.id)) {
      const timeLeft = await cdManager.getTimeLeft(this.name, player.id);
      throw new CommandError(`You are on cooldown. Please wait for ${timeLeft}`);
    }

    const loot = random.integer(this.minEarn, this.maxEarn);
    const embed = new MessageEmbed(msg.author);

    embed.setDescription(random.pick(this.lores));

    const menu = new ButtonHandler(msg, embed);

    let collected = false;

    menu.addButton("Collect", () => {
      collected = true;
    });

    await menu.run();

    if (!collected) {
      throw new CommandError("reward has not been claimed");
    }

    embed.setDescription(`You've earned ${loot} ${coin}`);
    this.sendEmbed(msg, embed);

    player.coins += loot;
    player.save();

    await handleRole(msg, player, embed);

    cdManager.setCooldown(this.name, player.id, { hours: 3 });
  }
}
