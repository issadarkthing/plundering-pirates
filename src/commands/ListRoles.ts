import { Message, PermissionResolvable } from "discord.js";
import { Command } from "@jiman24/commandment";
import { roleManager } from "../bot";
import { coin } from "../structure/constants";
import { MessageEmbed } from "../structure/MessageEmbed";

export default class extends Command {
  name = "list-roles";
  aliases = ["lr", "list-role"];
  permissions: PermissionResolvable[] = ["ADMINISTRATOR"];
  description = "List auto roles";

  async exec(msg: Message) {

    await msg.guild?.roles.fetch();

    const autoRoles = (await roleManager.getAll())
      .map(autoRole => {
        const role = msg.guild!.roles.cache.get(autoRole.id);

        if (!role) {
          return undefined;
        }

        return {
          role,
          ...autoRole,
        }
      });

    const lines: string[] = [];

    for (const autoRole of autoRoles) {

      if (!autoRole) continue;

      lines.push(`${autoRole.role} | ${autoRole.threshold} ${coin}`);
    }

    const embed = new MessageEmbed(msg.author);

    embed.setDescription(lines.join("\n"));

    this.sendEmbed(msg, embed);
  }
}
