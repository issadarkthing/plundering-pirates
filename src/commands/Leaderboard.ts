import { Message } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { playerManager } from "../bot";
import { coin } from "../structure/constants";
import { MessageEmbed } from "../structure/MessageEmbed";
import { toNList } from "@jiman24/discordjs-utils";
import chunk from "lodash.chunk";
import { isNumber } from "lodash";

interface Player {
  name: string;
  coins: number;
}

export default class extends Command {
  name = "leaderboard";
  aliases = ["l", "lb"];
  description = "Leaderboard of the richest players across the seas";

  async exec(msg: Message, args: string[]) {

    if (!args[0]) {
      throw new CommandError("you need to give page index");
    }

    const index = Number(args[0]);

    if (!isNumber(index)) {
      throw new CommandError("invalid number passed");
    }

    const embed = new MessageEmbed(msg.author);

    const players = (await playerManager.db.values) as Player[];
    const list = players
      .sort((a, b) => b.coins - a.coins)
      .map(x => `${x.name} | \`${x.coins} ${coin}\``);

    const pages = chunk(list, 10);
    const page = pages.at(index - 1);

    if (!page) {
      throw new CommandError(`page ${index} does not exists, please select page between 1-${pages.length}`)
    }


    embed.setDescription(toNList(list));

    this.sendEmbed(msg, embed);

  }
}
