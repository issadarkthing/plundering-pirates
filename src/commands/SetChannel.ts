import { Message, PermissionResolvable } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { settingsManager } from "../bot";
import { MessageEmbed } from "../structure/MessageEmbed";

export default class extends Command {
  name = "set-channel";
  description = "Sets which channel the bot will interact";
  permissions: PermissionResolvable[] = ["ADMINISTRATOR"];

  async exec(msg: Message) {

    const channel = msg.mentions.channels.first();

    if (!channel) {
      throw new CommandError("you need to mention a channel");
    } else if (!channel.isText()) {
      throw new CommandError("cannot set non text-based channel");
    }

    settingsManager.interactChannelID = channel.id;
    settingsManager.save();

    const embed = new MessageEmbed(msg.author)
      .setDescription(`successfully set ${channel} as the bot channel`);

    this.sendEmbed(msg, embed);
  }
}
