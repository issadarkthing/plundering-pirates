import { Command } from "@jiman24/commandment";
import { Message } from "discord.js";
import { Player } from "../structure/Player";
import { random } from "@jiman24/discordjs-utils";
import { MessageEmbed } from "../structure/MessageEmbed";
import { coin } from "../structure/constants";

export default class extends Command {
  name = "tavern";
  aliases = ["g"];
  description = "slot machine game";
  symbols = ["🔵", "🔴", "⚪"];
  cost = 5;
  rewards = [1, 2, 3, 4, 5, 10, 20, 50, 100];
  // cooldown?: Duration | undefined = { minute: 1 };

  private allEqual(arr: string[]) {
    return arr.every(x => x === arr[0]);
  }

  private getColumn(index: number, arr: string[][]) {
    return arr.map(x => x[index]);
  }

  private getCrosses(arr: string[][]) {
    return [
      [arr[0][0], arr[1][1], arr[2][2]],
      [arr[0][2], arr[1][1], arr[2][0]],
    ];
  }

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);

    const rows = Array(3)
      .fill(null)
      .map(() => Array(3).fill(null).map(() => random.pick(this.symbols)));

    let multiplier = 1;

    // row check
    for (const row of rows) {
      if (this.allEqual(row)) {
        multiplier++;
      }
    }

    // column check
    for (let i = 0; i < rows.length; i++) {
      const column = this.getColumn(i, rows);

      if (this.allEqual(column)) {
        multiplier++;
      }
    }

    // cross check
    for (const row of this.getCrosses(rows)) {
      if (this.allEqual(row)) {
        multiplier++;
      }
    }

    const result = rows
      .map(x => "**|** " + x.join("") + " **|**")
      .join("\n");

    const embed = new MessageEmbed(msg.author);
    let description = result;



    if (multiplier === 1) {

      player.coins -= this.cost;
      description += `\n---\nYou lost **${this.cost}** ${coin}!`;
      embed.setColor("RED");

    } else {
      const winAmount = this.rewards[multiplier];
      player.coins += winAmount;
      description += `\n---\nYou won **${winAmount}** ${coin}!`;
      embed.setColor("GREEN");
    }

    embed.setDescription(description);
    this.sendEmbed(msg, embed);

    player.save();
  }
}
