import { Message, PermissionResolvable } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { validateNumber } from "@jiman24/discordjs-utils";
import { MessageEmbed } from "../structure/MessageEmbed";
import { roleManager } from "../bot";

export default class extends Command {
  name = "add-role";
  aliases = ["ar"];
  description = "Add auto role";
  permissions: PermissionResolvable[] = ["ADMINISTRATOR"];

  async exec(msg: Message, args: string[]) {

    const amountStr = args[1];
    const amount = parseInt(amountStr);

    if (!args[0]) {
      throw new CommandError("you need to mention a role");
    } else if (!amountStr) {
      throw new CommandError("you need to give an amount");
    } 

    validateNumber(amount);

    if (amount < 1) {
      throw new CommandError("coin amount cannot be less than 1");
    }

    const role = msg.mentions.roles.first()!;

    roleManager.addRole({ id: role.id, threshold: amount });

    const embed = new MessageEmbed(msg.author);
    embed.setColor("GREEN");

    embed.setDescription(`Successfully set auto role for ${role}`);

    this.sendEmbed(msg, embed);
  }
}
