import { Message } from "discord.js";
import { Command } from "@jiman24/commandment";
import { Player } from "../structure/Player";
import { MessageEmbed } from "../structure/MessageEmbed";
import { coin } from "../structure/constants";

export default class extends Command {
  name = "balance";
  aliases = ["b"];
  description = "Check your balance";

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);

    const embed = new MessageEmbed(msg.author);
    embed.setDescription(`You have ${player.coins} ${coin}`);

    this.sendEmbed(msg, embed);

  }
}
