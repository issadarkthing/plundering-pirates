import { Message, PermissionResolvable } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { settingsManager } from "../bot";
import { MessageEmbed } from "../structure/MessageEmbed";

export default class extends Command {
  name = "auto-role";
  description = "Enable or disable auto-role";
  permissions: PermissionResolvable[] = ["ADMINISTRATOR"];

  async exec(msg: Message, args: string[]) {

    const embed = new MessageEmbed(msg.author);
    const arg1 = args[0]

    if (arg1) {

      if (arg1 !== "enable" && arg1 !== "disable") {
        throw new CommandError("invalid argument");
      }

      settingsManager.enableAutoRole = arg1 === "enable";
      settingsManager.save();

      embed.setColor("GREEN");
      embed.setDescription(`auto-role is now ${arg1}d`);

      this.sendEmbed(msg, embed);

      return;
    }

    const autoRoleStatus = settingsManager.enableAutoRole ? "enabled" : "disabled";
    const footerText = settingsManager.enableAutoRole ?
      "To disable auto-role, run !auto-role disable" :
      "To enable auto-role, run !auto-role enable";

    embed.setDescription(autoRoleStatus);
    embed.setFooter({ text: footerText });

    this.sendEmbed(msg, embed);
  }
}
